import os
import pickle
import numpy as np
import mlccd_models
import h5py
import pandas as pd
import mlccd_diffusion


class CCDData:
    """
    A class to represent CCD data.

    Attributes
    ----------
    file_path : str
        The path to the .h5 file containing the CCD data.
    threshold_low : float
        The lower threshold for clipping the data.
    threshold_high : float
        The upper threshold for clipping the data.
    train_test_validation_fraction : tuple
        A tuple of three floats representing the fraction of the data to be used for training, testing, and validation.
        The sum of the three fractions should be equal to 1.
    image_unit : str
        The unit of the images. Default is "charges".
    normalize : bool
        Whether to normalize the data.
    seed : int
        The seed for random number generation. Default is 42.
        If the seed is set to the same value, the random division
        of the data will be the same each time the class is instantiated.
    images : numpy.ndarray
        An array of images.
    labels : numpy.ndarray
        The labels for the images (0 for background, 1 for tritium).
    tracks_metadata : pandas.DataFrame
        A DataFrame containing the metadata for the tracks.
    diffuser_metadata : dict
        A dictionary containing the metadata for the diffuser.
    geant4_metadata : dict
        A dictionary containing the metadata for the Geant4 simulation.
    IMAGE_WIDTH : int
        The width of the images.
    IMAGE_HEIGHT : int
        The height of the images.
    IMAGE_CHANNELS : int
        The number of channels in the images.
    divided_train_test_validation : bool
        Whether the data has been divided into training, testing, and validation sets.
    train_indices : numpy.ndarray
        The indices of the training data.
    test_indices : numpy.ndarray
        The indices of the testing data.
    validation_indices : numpy.ndarray
        The indices of the validation data.
    images_original : numpy.ndarray
        The original images before normalization.
    """

    def __init__(
        self,
        file_path,
        threshold_low=None,
        threshold_high=None,
        train_test_validation_fraction=None,
        image_unit="keV",
        normalize=False,
        seed=42,
    ):
        self.seed = seed
        np.random.seed(self.seed)
        self.file_path = file_path
        self.divided_train_test_validation = False
        self.threshold_high = threshold_high
        self.threshold_low = threshold_low
        self.images = None
        self.labels = None
        self.tracks_metadata = None
        self.diffuser_metadata = {}
        self.geant4_metadata = {}
        self.image_unit = image_unit
        self.read()
        (_num_images, self.IMAGE_WIDTH, self.IMAGE_HEIGHT) = self.images.shape
        self.IMAGE_CHANNELS = 1  # grayscale images -> 1 channel
        self.reshape_data()
        if self.image_unit.lower() != "kev":
            self.convert_to_kev()
        if normalize:
            self.normalize(threshold_low, threshold_high)
        if train_test_validation_fraction:
            self.divide_data(train_test_validation_fraction)

    def __str__(self):
        return f"CCDData with {self.images.shape[0]} images from {self.geant4_metadata}"

    def __repr__(self):
        return f"CCDData(file_path={self.file_path}, threshold_low={self.threshold_low}, threshold_high={self.threshold_high})"

    def __eq__(self, other: object) -> bool:
        if not type(self) == type(other):
            return False
        return (
            self.file_path == other.file_path
            and np.array_equal(self.images, other.images)
            and np.array_equal(self.labels, other.labels)
            and self.threshold_high == other.threshold_high
            and self.threshold_low == other.threshold_low
        )

    def __add__(self, other):
        """
        Add two CCDData objects by concatenating their images, labels, and tracks_metadata.
        The rest of the attributes are taken from the first object.
        """
        if not isinstance(other, CCDData):
            raise ValueError("Can only add CCDData objects")

        new_obj = CCDData.__new__(CCDData)

        new_obj.tracks_metadata = pd.concat(
            [self.tracks_metadata, other.tracks_metadata], ignore_index=True
        )
        new_obj.images = np.concatenate([self.images, other.images], axis=0)
        new_obj.labels = np.concatenate([self.labels, other.labels], axis=0)
        new_obj.file_path = (
            [self.file_path, other.file_path]
            if isinstance(self.file_path, list)
            else self.file_path
        )
        for attr, value in self.__dict__.items():
            if attr not in ["tracks_metadata", "images", "labels", "file_path"]:
                setattr(new_obj, attr, value)
        return new_obj

    def __mul__(self, factor):
        """
        Reduce the number of events by a factor in place.
        """
        if not isinstance(factor, (int, float)):
            raise TypeError("Factor must be an integer or float")
        if factor <= 0 or factor > 1:
            raise ValueError("Factor must be between 0 and 1")

        num_events = int(len(self.images) * factor)
        return self.reduced(num_events, in_place=False)

    def __rmul__(self, factor):
        """
        Enable multiplication with the factor on the left-hand side.
        """
        return self.__mul__(factor)
    
    def write(self, file_path):
        """
        Save the CCDData object to an HDF5 file.
        """
        if not file_path.endswith('.h5'):
            file_path += '.h5'
        
        with h5py.File(file_path, 'w') as h5f:
            h5f.create_dataset('images', data=self.images.squeeze(axis=-1))
            self.tracks_metadata.creatorProcessesUnique = self.tracks_metadata.creatorProcessesUnique.astype('S20')
            self.tracks_metadata.stepKindsUnique = self.tracks_metadata.stepKindsUnique.astype('S10')
            tracks_metadata_records = self.tracks_metadata.to_records(index=False)
            h5f.create_dataset('tracks_metadata', data=tracks_metadata_records)
            # Write diffuser and geant4 metadata
            diffuser_metadata_group = h5f.create_group('diffuser_metadata')
            for key, value in self.diffuser_metadata.items():
                diffuser_metadata_group.attrs[key] = value
        
            geant4_metadata_group = h5f.create_group('geant4_metadata')
            for key, value in self.geant4_metadata.items():
                geant4_metadata_group.attrs[key] = value

    def read(self):
        if self.file_path.endswith('.h5'):
            self.read_h5()
        elif self.file_path.endswith('.pkl'):
            self.read_pkl()
        else:
            raise ValueError(f"Unsupported file format or directory structure: {self.file_path}")

    def read_pkl(self):
        """
        Load .pkl data from the file_path.
        """
        with open(self.file_path, 'rb') as f:
            data = pickle.load(f)
        self.images = np.concatenate([data['x_train'], data['x_test']])
        self.labels = np.concatenate([data['y_train'], data['y_test']])
        self.tracks_metadata = pd.DataFrame()
        self.diffuser_metadata = {}
        self.geant4_metadata = {}

    def read_h5(self):
        """
        Load .h5 data from the file_path.
        """
        with h5py.File(self.file_path, "r") as h5f:
            self.images = h5f["images"][:]
            summary_records = h5f["tracks_metadata"][:]
            self.tracks_metadata = pd.DataFrame.from_records(summary_records)
            diffuser_metadata_group = h5f["diffuser_metadata"]
            self.diffuser_metadata = {
                key: diffuser_metadata_group.attrs[key]
                for key in diffuser_metadata_group.attrs
            }
            geant4_metadata_group = h5f["geant4_metadata"]
            self.geant4_metadata = {
                key: geant4_metadata_group.attrs[key]
                for key in geant4_metadata_group.attrs
            }
        self.labels = (
            (self.tracks_metadata.primaryKind == 1000010030).astype(int).to_numpy()
        )

    def reduced(self, num_events, in_place=False):
        """
        Return a copy of the CCDData object with a specified number of randomly selected events.
        """
        if num_events > len(self.images):
            raise ValueError(
                "num_events is greater than the number of available events"
            )

        indices = np.random.choice(len(self.images), num_events, replace=False)
        original_simulated_events = int(self.geant4_metadata["numParticles"])
        new_geant4_metadata = self.geant4_metadata.copy()
        new_geant4_metadata["numParticles"] = (
            original_simulated_events * num_events // len(self.images)
        )

        if in_place:
            self.images = self.images[indices]
            self.labels = self.labels[indices]
            self.tracks_metadata = self.tracks_metadata.iloc[indices].reset_index(
                drop=True
            )
            self.geant4_metadata = new_geant4_metadata
        else:
            new_obj = CCDData.__new__(CCDData)

            # Copy all attributes from self to new_obj
            for attr, value in self.__dict__.items():
                if attr in ["images", "labels", "tracks_metadata", "geant4_metadata"]:
                    continue
                setattr(new_obj, attr, value)

            # Set the reduced attributes
            new_obj.images = self.images[indices]
            new_obj.labels = self.labels[indices]
            new_obj.tracks_metadata = self.tracks_metadata.iloc[indices].reset_index(
                drop=True
            )
            new_obj.geant4_metadata = new_geant4_metadata
            new_obj.file_path = (
                f"reduced_{num_events}_{self.file_path}"
                if not isinstance(self.file_path, list)
                else self.file_path
            )

            return new_obj


    def reshape_data(self):
        """
        Reshape the data to the desired shape (num_images, IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS).
        """
        desired_shape = (
            self.images.shape[0],
            self.IMAGE_WIDTH,
            self.IMAGE_HEIGHT,
            self.IMAGE_CHANNELS,
        )

        if self.images.shape != desired_shape:
            self.images = self.images.reshape(desired_shape)

    def convert_to_kev(self):
        """
        Convert the images to energy in keV.
        """
        energy_electron_hole_pair = 3.6e-3  # keV
        adc_per_electron = 350 # from Guillermo at Fermilab
        if self.image_unit.lower() == "electrons":
            self.images = self.images * energy_electron_hole_pair
        elif self.image_unit.lower() == "adc":
            self.images = self.images * energy_electron_hole_pair / adc_per_electron
        elif self.image_unit.lower() == "kev":
            pass
        else:
            raise ValueError(f"Invalid image unit: {self.image_unit}")
        self.image_unit = "keV"

    def normalize(self, threshold_low=None, threshold_high=None):
        """
        Normalize the images by clipping the values between threshold_low and threshold_high and scaling them between 0 and 1.

        Parameters
        ----------
        threshold_low : float
            The lower threshold for clipping the data.
        threshold_high : float
            The upper threshold for clipping the data.
        """
        if all(
            threshold is None
            for threshold in [
                self.threshold_low,
                self.threshold_high,
                threshold_low,
                threshold_high,
            ]
        ):
            print("Thresholds not set. Not normalizing.")
            return
        self.threshold_low = threshold_low
        self.threshold_high = threshold_high
        self.images_original = self.images.copy()
        self.images = np.clip(self.images, self.threshold_low, self.threshold_high)
        self.images = (self.images - self.threshold_low) / (
            self.threshold_high - self.threshold_low
        )

    def divide_data(self, train_fraction, test_fraction, validation_fraction):
        """
        Divide the data into training, testing, and validation sets.

        Parameters
        ----------
        train_fraction : float
            The fraction of the data to be used for training.
        test_fraction : float
            The fraction of the data to be used for testing.
        validation_fraction : float
            The fraction of the data to be used for validation.
        """
        if train_fraction + test_fraction + validation_fraction - 1.0 > 1e-9:
            raise ValueError(
                "The sum of train_fraction, test_fraction, and validation_fraction must be equal to 1."
            )

        num_images = self.images.shape[0]
        indices = np.arange(num_images)
        np.random.shuffle(indices)

        num_train = int(num_images * train_fraction)
        num_test = int(num_images * test_fraction)
        num_validation = num_images - num_train - num_test

        self.train_indices = indices[:num_train]
        self.test_indices = indices[num_train : num_train + num_test]
        self.validation_indices = indices[num_train + num_test :]

        self.divided_train_test_validation = True

    @property
    def x_train(self):
        return self.images[self.train_indices]

    @property
    def x_test(self):
        return self.images[self.test_indices]

    @property
    def x_validation(self):
        return self.images[self.validation_indices]

    @property
    def y_train(self):
        return self.labels[self.train_indices]

    @property
    def y_test(self):
        return self.labels[self.test_indices]

    @property
    def y_validation(self):
        return self.labels[self.validation_indices]

    def training_class_weights(self):
        """Calculate class weights for the training data."""
        class_counts = np.bincount(self.y_train.astype(int))
        num_negatives = class_counts[0]
        num_positives = class_counts[1]
        return {0: 1, 1: num_negatives / num_positives}

    @property
    def y_test_inverted(self):
        """Returns the inverted values of y_test (1 - y_test)."""
        return 1 - self.y_test

    @property
    def y_train_inverted(self):
        """Returns the inverted values of y_train (1 - y_train)."""
        return 1 - self.y_train

    @property
    def x_test_bkg_indices(self):
        """Returns the indices of x_test where y_test is 0 (background)."""
        return np.where(self.y_test == 0)[0].tolist()

    @property
    def x_test_tritium_indices(self):
        """Returns the indices of x_test where y_test is 1 (tritium)."""
        return np.where(self.y_test == 1)[0].tolist()

    @property
    def x_test_bkg(self):
        """Returns the x_test samples corresponding to background (y_test == 0)."""
        return self.x_test[self.x_test_bkg_indices]

    @property
    def x_test_tritium(self):
        """Returns the x_test samples corresponding to tritium (y_test == 1)."""
        return self.x_test[self.x_test_tritium_indices]

    @property
    def x_train_bkg_indices(self):
        """Returns the indices of x_train where y_train is 0 (background)."""
        return np.where(self.y_train == 0)[0].tolist()

    @property
    def x_train_tritium_indices(self):
        """Returns the indices of x_train where y_train is 1 (tritium)."""
        return np.where(self.y_train == 1)[0].tolist()

    @property
    def x_train_bkg(self):
        """Returns the x_train samples corresponding to background (y_train == 0)."""
        return self.x_train[self.x_train_bkg_indices]

    @property
    def x_train_tritium(self):
        """Returns the x_train samples corresponding to tritium (y_train == 1)."""
        return self.x_train[self.x_train_tritium_indices]

    def plot(self, index=1, title="", only_one_label=None, **kwargs):
        """Plots the side-by-side comparison of x_train_kev and x_train."""
        images = self.images
        if only_one_label is not None:
            images = images[self.labels == only_one_label]
        mlccd_models.plot_12(images, unit=self.image_unit, title=title, **kwargs)

    def __getitem__(self, key):
        """
        Allow indexing of CCDData object with primaryID to get the corresponding image(s).
        
        Parameters:
        -----------
        key : int or list of ints
            The primaryID(s) to retrieve images for.
        
        Returns:
        --------
        numpy.ndarray
            The image(s) corresponding to the given primaryID(s).
        """
        if isinstance(key, (int, np.integer)):
            # Single primaryID
            idx = np.where(self.tracks_metadata['primaryID'] == key)[0]
            if len(idx) == 0:
                raise KeyError(f"No image found with primaryID {key}")
            return self.images[idx[0]]
        elif isinstance(key, (list, np.ndarray)):
            # List of primaryIDs
            idx = np.isin(self.tracks_metadata['primaryID'], key)
            if not np.any(idx):
                raise KeyError(f"No images found with the given primaryIDs")
            return self.images[idx]
        else:
            raise TypeError("Key must be an integer or a list of integers (primaryIDs)")







