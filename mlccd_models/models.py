import tensorflow as tf
from tensorflow.keras import layers
import numpy as np


class Sampling(layers.Layer):
    """Enables autoencoders to perform reparameterization by sampling
    from an isotropic unit Gaussian."""

    def call(self, inputs):
        """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


class VAE(tf.keras.Model):
    def __init__(
        self,
        encoder,
        decoder,
        IMAGE_WIDTH=128,
        IMAGE_HEIGHT=128,
        kl_loss_weight=1.0,
        mse_scaler=5000,
        l1_weight=0.01,  
        **kwargs,
    ):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.IMAGE_WIDTH = IMAGE_WIDTH
        self.IMAGE_HEIGHT = IMAGE_HEIGHT
        self.kl_loss_weight = kl_loss_weight
        self.mse_scaler = mse_scaler
        self.l1_weight = l1_weight  

    def train_step(self, data):
        if isinstance(data, tuple):
            x_val, _y_val = data
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(x_val)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                tf.keras.losses.binary_crossentropy(x_val, reconstruction)
            )
            reconstruction_loss *= self.IMAGE_WIDTH * self.IMAGE_HEIGHT
            kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
            kl_loss = tf.reduce_mean(kl_loss)
            kl_loss *= -0.5
            
            # Add L1 regularization
            l1_loss = tf.add_n([tf.reduce_sum(tf.abs(w)) for w in self.trainable_weights])
            
            total_loss = reconstruction_loss + kl_loss * self.kl_loss_weight + self.l1_weight * l1_loss

        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))

        return {
            "loss": total_loss,
            "reconstruction_loss": reconstruction_loss,
            "kl_loss": kl_loss,
            "l1_loss": l1_loss,  
        }

    def test_step(self, data):
        x_val, _y_val = data
        z_mean, z_log_var, z = self.encoder(x_val)
        reconstruction = self.decoder(z)
        reconstruction_loss = tf.reduce_mean(
            tf.keras.losses.binary_crossentropy(x_val, reconstruction)
        )
        reconstruction_loss *= self.IMAGE_WIDTH * self.IMAGE_HEIGHT
        kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
        kl_loss = tf.reduce_mean(kl_loss)
        kl_loss *= -0.5
        total_loss = reconstruction_loss + kl_loss * self.kl_loss_weight
        return {
            "loss": total_loss,
            "reconstruction_loss": reconstruction_loss,
            "kl_loss": kl_loss,
        }

    def predict(self, x):
        """Predict the reconstruction error for a given set of images.
        Returns the exponential of the negative mean squared error so that values
        are mapped to the range (0, 1) where 1 is a perfect reconstruction.
        The mse_scaler is used to scale the mse values to a range that is more
        suitable for the exponential function. For keV images, the mse is on
        the orger of 0.00001.
        NOTE: the exponential is preffered to for instance a sigmoid function
        because it has higher sensitivity to mse-values close to 0.
        """
        z_mean, _, z = self.encoder(x)
        reconstruction = self.decoder(z)
        mse = tf.reduce_mean(tf.square(x - reconstruction), axis=(1, 2, 3)).numpy()
        return np.exp(-mse * self.mse_scaler)

    def encode(self, x):
        """Encode input images to the latent space."""
        z_mean, z_log_var, _ = self.encoder(x)
        return z_mean.numpy()

    def predict_euclidean(self, x):
        """Predict anomaly scores using Euclidean distance in the latent space."""
        encoded = self.encode(x)
        distances = np.linalg.norm(encoded, axis=1)
        return np.exp(-distances)

    def predict_mahalanobis(self, x):
        """Predict anomaly scores using Mahalanobis distance in the latent space."""
        encoded = self.encode(x)
        cov_matrix = np.cov(encoded.T)
        distances = np.array([
            np.sqrt(sample.dot(np.linalg.inv(cov_matrix)).dot(sample))
            for sample in encoded
        ])
        return np.exp(-distances)

    def predict_log_likelihood(self, x):
        """Predict anomaly scores using log-likelihood in the latent space."""
        encoded = self.encode(x)
        mean = np.mean(encoded, axis=0)
        cov_matrix = np.cov(encoded.T)
        log_likelihoods = np.array([
            -0.5 * np.log(np.linalg.det(cov_matrix)) - 
            0.5 * (sample - mean).dot(np.linalg.inv(cov_matrix)).dot(sample - mean)
            for sample in encoded
        ])
        return np.exp(log_likelihoods)

    # Update get_config and from_config methods
    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "encoder": self.encoder.get_config(),
                "decoder": self.decoder.get_config(),
                "IMAGE_WIDTH": self.IMAGE_WIDTH,
                "IMAGE_HEIGHT": self.IMAGE_HEIGHT,
                "kl_loss_weight": self.kl_loss_weight,
                "l1_weight": self.l1_weight,  
            }
        )
        return config

    @classmethod
    def from_config(cls, config):
        encoder_config = config.pop("encoder")
        decoder_config = config.pop("decoder")
        encoder = VAEEncoder.from_config(encoder_config)
        decoder = VAEDecoder.from_config(decoder_config)
        return cls(encoder, decoder, **config)

    def call(self, inputs):
        z_mean, z_log_var, z = self.encoder(inputs)
        reconstructed = self.decoder(z)
        return reconstructed


def VAEModel(
    IMAGE_WIDTH,
    IMAGE_HEIGHT,
    IMAGE_CHANNELS,
    pinch_size,
    kl_loss_weight=1.0,
    l1_weight=0.01,  
    mse_scaler=5000,
    padding="same",
    name="VAE",
):
    """Variational Autoencoder model for tritium images.

    Parameters
    ----------
        IMAGE_WIDTH : int
            Width of the image [pixels]
        IMAGE_HEIGHT : int
            Height of the image [pixels]
        IMAGE_CHANNELS : int
            Number of channels in the image.
            Typically 1 for grayscale CCD particle tracks
        pinch_size : int
            Size of the bottleneck layer in the VAE model. A smaller
            value will force the model to learn a more compressed
            representation of the input.
        kl_loss_weight : float
            Weight for the Kullback-Leibler loss term in the VAE model.
            Higher values will force the model to stay within a narrow
            range of the latent space. Setting this value to 0 will
            effectively make this a standard autoencoder.
        l1_weight : float
            Weight for the L1 regularization term in the VAE model.
            Higher values will enforce sparsity in the learned representations.
        mse_scaler : float
            Scaler for the mean squared error loss term in the VAE model.
            This is used to scale the mse values to a range that is more
            suitable for the exponential function. For keV images, the mse
            is on the order of 0.00001.
        padding : str
            Padding for the Conv2D layers in the model. Options are
            'same' and 'valid'. 'same' will keep the output size the
            same as the input size, while 'valid' will reduce the
            output size making more of an hourglass-shaped model.
        name : str
            Name of the model

    Returns
    -------
        vae : tf.keras.Model
            Variational Autoencoder model
    """
    encoder = VAEEncoder(
        IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS, pinch_size, padding=padding
    )
    decoder = VAEDecoder(
        IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS, pinch_size, padding=padding
    )
    vae = VAE(encoder, decoder, IMAGE_WIDTH, IMAGE_HEIGHT, kl_loss_weight, l1_weight=l1_weight, name=name, mse_scaler=mse_scaler)
    return vae


class VAEEncoder(tf.keras.Model):
    def __init__(
        self,
        IMAGE_WIDTH,
        IMAGE_HEIGHT,
        IMAGE_CHANNELS,
        pinch_size,
        padding="same",
        **kwargs,
    ):
        super(VAEEncoder, self).__init__(**kwargs)
        self.IMAGE_WIDTH = IMAGE_WIDTH
        self.IMAGE_HEIGHT = IMAGE_HEIGHT
        self.IMAGE_CHANNELS = IMAGE_CHANNELS
        self.pinch_size = pinch_size
        self.padding = padding

        self.encoder_inputs = tf.keras.Input(
            shape=(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS)
        )
        x = layers.Conv2D(8, kernel_size=3, activation="relu", padding=padding)(
            self.encoder_inputs
        )
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)
        x = layers.Conv2D(16, kernel_size=3, activation="relu", padding=padding)(x)
        x = layers.Flatten()(x)
        self.z_mean = layers.Dense(pinch_size)(x)
        self.z_log_var = layers.Dense(pinch_size)(x)
        z = Sampling()([self.z_mean, self.z_log_var])
        self.encoder = tf.keras.Model(
            self.encoder_inputs, [self.z_mean, self.z_log_var, z], name="encoder"
        )

    def call(self, inputs):
        return self.encoder(inputs)

    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "IMAGE_WIDTH": self.IMAGE_WIDTH,
                "IMAGE_HEIGHT": self.IMAGE_HEIGHT,
                "IMAGE_CHANNELS": self.IMAGE_CHANNELS,
                "pinch_size": self.pinch_size,
                "padding": self.padding,
            }
        )
        return config

    @classmethod
    def from_config(cls, config):
        return cls(**config)


class VAEDecoder(tf.keras.Model):
    def __init__(
        self,
        IMAGE_WIDTH,
        IMAGE_HEIGHT,
        IMAGE_CHANNELS,
        pinch_size,
        padding="same",
        **kwargs,
    ):
        super(VAEDecoder, self).__init__(**kwargs)
        self.IMAGE_WIDTH = IMAGE_WIDTH
        self.IMAGE_HEIGHT = IMAGE_HEIGHT
        self.IMAGE_CHANNELS = IMAGE_CHANNELS
        self.pinch_size = pinch_size
        self.padding = padding

        self.latent_inputs = tf.keras.Input(shape=(pinch_size,))
        if padding == "same":
            x = layers.Dense(
                (IMAGE_WIDTH // 2) * (IMAGE_HEIGHT // 2) * 16, activation="relu"
            )(self.latent_inputs)
            x = layers.Reshape(((IMAGE_WIDTH // 2), (IMAGE_HEIGHT // 2), 16))(x)
        elif padding == "valid":
            x = layers.Dense(
                ((IMAGE_WIDTH // 4) - 1) * ((IMAGE_HEIGHT // 4) - 1) * 16,
                activation="relu",
            )(self.latent_inputs)
            x = layers.Reshape(
                (((IMAGE_WIDTH // 4) - 1), ((IMAGE_HEIGHT // 4) - 1), 16)
            )(x)
        else:
            raise ValueError(f"Invalid padding value: {padding}")
        x = layers.Conv2DTranspose(
            16, kernel_size=3, activation="relu", padding=padding
        )(x)
        x = layers.UpSampling2D((2, 2))(x)
        x = layers.Conv2DTranspose(
            8, kernel_size=3, activation="relu", padding=padding
        )(x)
        self.decoder_outputs = layers.Conv2DTranspose(
            IMAGE_CHANNELS, kernel_size=3, activation="relu", padding=padding
        )(x)
        self.decoder = tf.keras.Model(
            self.latent_inputs, self.decoder_outputs, name="decoder"
        )

    def call(self, inputs):
        return self.decoder(inputs)

    def get_config(self):
        config = super().get_config()
        config.update(
            {
                "IMAGE_WIDTH": self.IMAGE_WIDTH,
                "IMAGE_HEIGHT": self.IMAGE_HEIGHT,
                "IMAGE_CHANNELS": self.IMAGE_CHANNELS,
                "pinch_size": self.pinch_size,
                "padding": self.padding,
            }
        )
        return config

    @classmethod
    def from_config(cls, config):
        return cls(**config)


def AutoencoderModel(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS, pinch_size):
    """Autoencoder model for tritium images.

    Parameters
    ----------
    IMAGE_WIDTH : int
        Width of the image [pixels]
    IMAGE_HEIGHT : int
        Height of the image [pixels]
    IMAGE_CHANNELS : int
        Number of channels in the image.
        Typically 1 for grayscale CCD particle tracks
    pinch_size : int
        Size of the bottleneck layer in the Autoencoder model. A smaller
        value will force the model to learn a more compressed
        representation of the input.

    Returns
    -------
    model : tf.keras.Model
        Autoencoder model
    """
    model = tf.keras.Sequential()

    # Encoder layers
    model.add(
        tf.keras.layers.Conv2D(
            8,
            kernel_size=3,
            activation="relu",
            input_shape=(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS),
            padding="same",
        )
    )
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(
        tf.keras.layers.Conv2D(16, kernel_size=3, activation="relu", padding="same")
    )

    # Flatten and bottleneck here
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(units=pinch_size, activation="relu"))

    # Decoder layers
    model.add(
        tf.keras.layers.Dense(
            units=(IMAGE_WIDTH // 2) * (IMAGE_HEIGHT // 2) * 16, activation="relu"
        )
    )  # Adjust units to match the shape before flattening
    model.add(tf.keras.layers.Reshape(((IMAGE_WIDTH // 2), (IMAGE_HEIGHT // 2), 16)))
    model.add(
        tf.keras.layers.Conv2DTranspose(
            16, kernel_size=3, activation="relu", padding="same"
        )
    )
    model.add(tf.keras.layers.UpSampling2D((2, 2)))
    model.add(
        tf.keras.layers.Conv2DTranspose(
            8, kernel_size=3, activation="relu", padding="same"
        )
    )

    # Output layer to reconstruct the image
    model.add(
        tf.keras.layers.Conv2DTranspose(
            IMAGE_CHANNELS, kernel_size=3, activation="relu", padding="same"
        )
    )

    return model


def CNNModel(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS):
    inputs = tf.keras.layers.Input(shape=(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS))

    x = tf.keras.layers.Conv2D(
        filters=8,
        kernel_size=3,
        strides=1,
        activation="relu",
        kernel_initializer=tf.keras.initializers.VarianceScaling(),
    )(inputs)
    x = tf.keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(x)

    x = tf.keras.layers.Conv2D(
        filters=16,
        kernel_size=3,
        strides=1,
        activation="relu",
        kernel_initializer=tf.keras.initializers.VarianceScaling(),
    )(x)

    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(units=128, activation="relu")(x)
    x = tf.keras.layers.Dropout(0.2)(x)

    outputs = tf.keras.layers.Dense(
        units=1,
        activation="sigmoid",
        kernel_initializer=tf.keras.initializers.VarianceScaling(),
    )(x)

    model = tf.keras.Model(inputs=inputs, outputs=outputs, name="CNN")
    return model
