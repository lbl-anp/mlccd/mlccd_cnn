import wandb
import os
import yaml
import argparse

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Suppress TensorFlow logging (errors only)
import tensorflow as tf
tf.config.set_visible_devices([], "GPU")

def main(sweep_config_path):
    # Load sweep config from YAML file
    with open(sweep_config_path, 'r') as file:
        sweep_configuration = yaml.safe_load(file)

    # Initialize the sweep
    sweep_id = wandb.sweep(sweep_configuration, project="sim_models")

    # Run the agent
    wandb.agent(sweep_id, count=None)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run a W&B sweep with a specified config file.")
    parser.add_argument("config_file", help="Path to the sweep configuration YAML file")
    args = parser.parse_args()

    main(args.config_file)