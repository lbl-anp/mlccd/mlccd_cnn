from matplotlib.colors import LinearSegmentedColormap
from sklearn.metrics import roc_curve, auc
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
import tensorflow as tf
from matplotlib import gridspec


def side_by_side(left_images, right_images, i=0, title_left="", title_right=""):
    fig = plt.figure(figsize=(5, 1.5), dpi=150)
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.set_title(title_left)
    ax1.axis("off")  # Remove axis labels
    im1 = ax1.imshow(left_images[i, :, :])
    fig.colorbar(im1, ax=ax1)

    ax2 = fig.add_subplot(1, 2, 2)
    ax2.set_title(title_right)
    ax2.axis("off")  # Remove axis labels
    im2 = ax2.imshow(right_images[i, :, :, 0])
    fig.colorbar(im2, ax=ax2)


def plot_12(images, num_plots=16, unit="keV", title="", vmin=None, vmax=None):
    # Determine the number of rows and columns
    num_cols = int(np.sqrt(num_plots))
    num_rows = num_cols

    # Create a grid with an extra column for the gap and the colorbar
    fig = plt.figure(figsize=(3, 3), dpi=150)
    fig.suptitle(title)
    gs = gridspec.GridSpec(
        num_rows, num_cols + 2, width_ratios=[1] * num_cols + [0.1, 0.1]
    )

    # Find the global min and max values across all images
    if vmin is None:
        vmin = np.min(images[:num_plots, :, :])
    if vmax is None:
        vmax = np.max(images[:num_plots, :, :])

    # Plot the images
    for i in range(num_plots):
        ax = fig.add_subplot(gs[i // num_cols, i % num_cols])
        if i < len(images):
            im = ax.imshow(images[i, :, :], aspect="auto", vmin=vmin, vmax=vmax)
        ax.axis("off")  # Remove axis labels

    # Add a colorbar to the right of the grid
    cbar_ax = fig.add_subplot(gs[:, -1])
    cbar = fig.colorbar(im, cax=cbar_ax, label=f"Energy ({unit})", shrink=0.8)
    cbar.ax.tick_params(labelsize=8)  # Adjust colorbar tick size

    plt.subplots_adjust(wspace=0, hspace=-0.001)  # Remove gaps between subplots
    plt.show()


def plot_confusion_matrix(y_true, y_pred, threshold=0.5, class_names = ["Background", "Tritium"]):
    # Calculate the confusion matrix
    # Threshold the predictions to binary values
    binary_predictions = tf.cast(y_pred >= threshold, tf.int32)
    confusion_matrix = tf.math.confusion_matrix(y_true, binary_predictions).numpy()
    # Normalize by row sums
    row_sums = confusion_matrix.sum(axis=1)
    normalized_confusion_matrix = confusion_matrix / row_sums[:, np.newaxis]

    labels = np.array([["TN", "FP"], ["FN", "TP"]])
    annot = np.array(
        [
            [
                f"{labels[i, j]}\n{normalized_confusion_matrix[i, j]:.3f}"
                for j in range(2)
            ]
            for i in range(2)
        ]
    )

    # Custom colormap from red to blue
    colors = [(240 / 255, 70 / 255, 50 / 255), (20 / 255, 90 / 255, 160 / 255)]

    n_bins = 100
    cmap_name = "custom_div_cmap"
    cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

    fig, ax = plt.subplots(figsize=(3.3, 2.5), dpi=150)
    sn.heatmap(
        normalized_confusion_matrix,
        annot=annot,
        linewidths=0.5,
        fmt="s",  # string formatting
        square=True,
        ax=ax,
        cmap=cm,
        cbar=False,
    )
    plt.xticks([0.5, 1.5], class_names)
    plt.yticks([0.5, 1.5], class_names)
    plt.xlabel("\nPredicted")
    plt.ylabel("True\n")
    plt.tight_layout()
    return fig

def plot_confusion_matrix_clusters(TP, FP, TN, FN, num_plots=16, title=None):
    # Determine the number of rows and columns
    num_cols = int(np.sqrt(num_plots))
    num_rows = num_cols

    # Create a grid
    fig = plt.figure(figsize=(5, 5), dpi=150)
    if title:
        fig.suptitle(title, fontsize=16)
    gs = gridspec.GridSpec(num_rows * 2, num_cols * 2)

    # Find the global min and max values across all images
    all_images = np.concatenate((TP, FP, TN, FN))
    vmin = np.min(all_images)
    vmax = np.max(all_images)

    # Define the quadrants and their positions
    quadrants = [
        (TP, 0, 0),
        (FN, 0, num_cols),
        (FP, num_rows, 0),
        (TN, num_rows, num_cols)
    ]

    # Plot the images
    for images, row_start, col_start in quadrants:
        for i in range(num_plots):
            row = row_start + i // num_cols
            col = col_start + i % num_cols
            ax = fig.add_subplot(gs[row, col])
            if i < len(images):
                ax.imshow(images[i], aspect='equal', vmin=vmin, vmax=vmax, interpolation='nearest')
            ax.axis('off')  # Remove axis labels

    plt.subplots_adjust(wspace=0, hspace=0)  # Remove gaps between subplots
    return fig

def plot_prediction_grid(ccd_data, predictions, low_threshold, high_threshold, vmin=None, vmax=None):
    quad_size = 5
    grid_size = 2 * quad_size
    fig, ax = plt.subplots(figsize=(6, 5), dpi=100)

    predictions = predictions.flatten()
    # Categorize predictions
    predictions_categorical = np.zeros_like(predictions, dtype=int)
    predictions_categorical[predictions > high_threshold] = 1
    predictions_categorical[(predictions >= low_threshold) & (predictions <= high_threshold)] = 2

    true_negatives = np.where((predictions_categorical == 0) & (ccd_data.y_test == 0))[0]
    false_positives = np.where((predictions_categorical == 1) & (ccd_data.y_test == 0))[0]
    false_negatives = np.where((predictions_categorical == 0) & (ccd_data.y_test == 1))[0]
    true_positives = np.where((predictions_categorical == 1) & (ccd_data.y_test == 1))[0]
    uncertain = np.where(predictions_categorical == 2)[0]
    quadrants = [
        (true_negatives, "Blues"),  # Top left
        (false_positives, "Reds"),  # Top right
        (false_negatives, "Reds"),  # Bottom left
        (true_positives, "Blues"),  # Bottom right
    ]

    for q_index, (quad_data, color_map) in enumerate(quadrants):
        row_offset = quad_size * (q_index // 2)
        col_offset = quad_size * (q_index % 2)

        numbers_to_display = min(quad_size * quad_size, len(quad_data))
        for plot_index in range(numbers_to_display):
            idx = quad_data[plot_index]
            sub_row = plot_index // quad_size
            sub_col = plot_index % quad_size
            
            # Calculate the position of each image in the grid
            x = col_offset + sub_col
            y = grid_size - (row_offset + sub_row) - 1  # Invert y-axis
            
            # Plot the image in the correct position
            ax.imshow(ccd_data.x_test[idx].reshape((ccd_data.IMAGE_WIDTH, ccd_data.IMAGE_HEIGHT)),
                      cmap=color_map, extent=[x, x+1, y, y+1], vmin=vmin, vmax=vmax)

    # Set the limits and remove ticks
    ax.set_xlim(0, grid_size)
    ax.set_ylim(0, grid_size)
    ax.set_xticks([])
    ax.set_yticks([])

    # Add grid lines
    for i in range(grid_size + 1):
        if i % quad_size == 0:
            # Quadrant-separating lines
            ax.axhline(y=i, color='black', linewidth=0.8)
            ax.axvline(x=i, color='black', linewidth=0.8)
        else:
            # Inner grid lines
            ax.axhline(y=i, color='gray', linewidth=0.2, alpha=0.5)
            ax.axvline(x=i, color='gray', linewidth=0.2, alpha=0.5)
    # Add labels
    ax.text(-1, grid_size / 2, 'True', va='center', ha='center', rotation=90, fontsize=14)
    ax.text(grid_size / 2, -1.2, 'Predicted', va='center', ha='center', fontsize=14)
    
    ax.text(-0.3, grid_size * 3/4, 'Background', va='center', ha='center', rotation=90, fontsize=14)
    ax.text(-0.3, grid_size * 1/4, 'Tritium', va='center', ha='center', rotation=90, fontsize=14)
    
    ax.text(grid_size * 1/4, -0.4, 'Background', va='center', ha='center', fontsize=14)
    ax.text(grid_size * 3/4, -0.4, 'Tritium', va='center', ha='center', fontsize=14)

    plt.tight_layout()
    return fig


def plot_roc_curve(y_true, y_scores, ax=None, label=None, log_scale=False, grid=False):
    lw = 2
    if ax is None:
        fig, ax = plt.subplots(figsize=(4, 3), dpi=150)
        x = np.logspace(-4, 1, 400)  # extend the range of x values
        y = x
        ax.plot(x, y, color="black", lw=lw, linestyle="--", label="Random")
        ax.set_xlim([0.0, 1.0])
        if log_scale:
            ax.set_xlim([1e-4, 1.0])
            ax.set_xscale("log")
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel("False Positive Rate")
        ax.set_ylabel("True Positive Rate")
        ax.set_title("Receiver Operating Characteristic")
        if grid:
            plt.grid()
    else:
        fig = ax.get_figure()
    # Compute ROC curve and ROC area for each class
    fpr, tpr, thresholds = roc_curve(y_true, y_scores)
    roc_auc = auc(fpr, tpr)

    # Find the closest threshold to 0.5
    closest_threshold_index = np.argmin(np.abs(thresholds - 0.5))
    closest_fpr = fpr[closest_threshold_index]
    closest_tpr = tpr[closest_threshold_index]
    # Add a marker for the 0.5 threshold
    # plt.plot(closest_fpr, closest_tpr, 'x', markersize=10,
    # label="Threshold 0.5", color='black', mew=2, zorder=10)
    if label is None:
        label = "ROC, area = %0.2f" % roc_auc
    else:
        label = f"{label}"#, area = {roc_auc:.2f}"

    ax.plot(fpr, tpr, lw=lw, label=label)

    ax.legend(loc="center left", bbox_to_anchor=(1.05, 0.5))
    return fig, ax

def plot_scores(y_true, y_scores, threshold_mcc_optimal=None, threshold_mda_optimal=None):
    bins = np.linspace(y_scores.min(), y_scores.max(), 100)
    fig, ax = plt.subplots(figsize=(5.5, 3), dpi=150)
    plt.hist(
        y_scores[y_true == 1],
        bins=bins,
        alpha=0.5,
        label="Tritium",
    )
    plt.hist(
        y_scores[y_true == 0],
        bins=bins,
        alpha=0.5,
        label="Background",
    )
    if threshold_mcc_optimal is not None:
        ax.axvline(
            x=threshold_mcc_optimal, color="r", linestyle="--", label="MCC-optimal threshold"
        )
    if threshold_mda_optimal is not None:
        ax.axvline(
            x=threshold_mda_optimal, color="g", linestyle=":", label="MDA-optimal\nthreshold"
        )
    plt.legend(loc="center left", bbox_to_anchor=(1, 0.5))
    plt.xlabel("Prediction score")
    plt.ylabel("Frequency")
    plt.yscale("log")
    fig.tight_layout()
    # plt.xticks(rotation=45)
    return fig, ax
