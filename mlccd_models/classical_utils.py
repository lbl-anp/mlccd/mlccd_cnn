import numpy as np
import pandas as pd
import mlccd_diffusion


def threshold_image(image, pixel_threshold_keV=None):
    """
    Takes an image and returns a list of the pixels that areabove threshold.
    If pixel_threshold_keV is None, use the default threshold of 4 times the Fermilab noise level.
    """
    if pixel_threshold_keV is None:
        pixel_threshold_keV = 4 * mlccd_diffusion.fermilab_noise_level(unit='keV')
    pixel_list = []
    if len(image) < 1:
        print("empty cluster")
        return
    for ix in range(len(image)):
        for iy in range(len(image[0])):
            if image[ix][iy] > pixel_threshold_keV:
                pixel_list.append([ix, iy, image[ix][iy]])

    return np.array(pixel_list)


def process_cluster(
    cluster_image, cluster_index, pixel_threshold_keV=None
):
    """
    Process a single cluster image and return a dictionary with the cluster metadata and a dataframe with the cluster properties.
    This has been replaced by the mlccd_diffusion.cluster_sigma function
    but is kept for now for comparison and testing.
    If pixel_threshold_keV is None, use the default threshold of 4 times the Fermilab noise level.
    """
    if pixel_threshold_keV is None:
        pixel_threshold_keV = 4 * mlccd_diffusion.fermilab_noise_level(unit='keV')
    if len(cluster_image.shape) == 3:  ##not yet reduced to single "color"
        cluster_image = cluster_image[:, :, 0]
    cluster_array = threshold_image(cluster_image, pixel_threshold_keV)
    if len(cluster_array) == 0:
        empty_cluster = {
            "sum": 0,
            "clusterEnergy": 0,
            "energy_per_pixel": 0,
            "clusterSigmaX": 0,
            "clusterSigmaY": 0,
            "clusterMinSigma": 0,
            "clusterMaxSigma": 0,
            "center_x": 0,
            "center_y": 0,
            "quadrant": 0,
            "clusterWidthX": 0,
            "clusterWidthY": 0,
            "clusterNpix": 0,
            "truthID": -1,
            "tritium_category": -1,
            "pixel_array": [],
        }
        empty_df = pd.DataFrame(
            {
                "clusterEnergy": [0],
                "clusterSigmaX": [0],
                "clusterSigmaY": [0],
                "clusterMinSigma": [0],
                "clusterMaxSigma": [0],
                "clusterNpix": [0],
                "clusterWidthX": [0],
                "clusterWidthY": [0],
            }
        )
        return empty_cluster, empty_df
    # cluster_array = cluster["pixel_list"]
    index_of_max_pixel = np.argmax(cluster_array[:, 2])
    center_x = cluster_array[index_of_max_pixel][0]
    min_x = np.min(cluster_array[:, 0]) - center_x
    max_x = np.max(cluster_array[:, 0]) - center_x
    center_y = cluster_array[index_of_max_pixel][1]
    min_y = np.min(cluster_array[:, 1]) - center_y
    max_y = np.max(cluster_array[:, 1]) - center_y

    width_x = int(max_x - min_x + 1)
    width_y = int(max_y - min_y + 1)

    quadrant = -999

    calibrated_cluster = []  # cluster.copy()
    sum_weights = 0
    mean_x = 0
    mean_y = 0
    var_x = 0
    var_y = 0
    for pixel in cluster_array:
        this_x = int(pixel[0] - center_x)
        this_y = int(pixel[1] - center_y)
        this_energy = pixel[2]

        sum_weights += this_energy
        mean_x += (this_x) * this_energy
        mean_y += (this_y) * this_energy
        var_x += (this_x) ** 2 * this_energy
        var_y += (this_y) ** 2 * this_energy

        calibrated_cluster.append(
            [
                this_x,
                this_y,
                this_energy,
                int(pixel[0]),
                int(pixel[1]),
                pixel[2],
                int(cluster_index),
            ]
        )

    mean_x /= sum_weights
    mean_y /= sum_weights
    var_x /= sum_weights
    var_y /= sum_weights
    var_x -= mean_x**2
    var_y -= mean_y**2
    sigma_x = np.sqrt(var_x)
    sigma_y = np.sqrt(var_y)

    minSigma = min(sigma_x, sigma_y)
    maxSigma = max(sigma_x, sigma_y)
    calibrated_cluster = np.array(calibrated_cluster)
    n_pixels = len(calibrated_cluster)

    energy = sum_weights
    energy_per_pixel = energy / float(n_pixels)

    cluster_dict = {
        "sum": sum_weights,
        "clusterEnergy": energy,
        "energy_per_pixel": energy_per_pixel,
        "clusterSigmaX": sigma_x,
        "clusterSigmaY": sigma_y,
        "clusterMinSigma": minSigma,
        "clusterMaxSigma": maxSigma,
        "center_x": center_x,
        "center_y": center_y,
        "quadrant": quadrant,
        "clusterWidthX": width_x,
        "clusterWidthY": width_y,
        "clusterNpix": n_pixels,
        "pixel_array": calibrated_cluster,
    }

    df = pd.DataFrame(
        {
            "clusterEnergy": [energy],
            "clusterSigmaX": [sigma_x],
            "clusterSigmaY": [sigma_y],
            "clusterMinSigma": [minSigma],
            "clusterMaxSigma": [maxSigma],
            "clusterNpix": [n_pixels],
            "clusterWidthX": [width_x],
            "clusterWidthY": [width_y],
        }
    )

    # isMC = cluster["MC"]
    # if not isMC:
    #     metadata = {"MC":False,"BG":cluster["BG"],"file_num":cluster["file_num"]}
    # else:
    #     metadata = {"MC":True}

    # cluster_dict = cluster_dict | metadata
    return cluster_dict, df


def add_cluster_metadata_legacy(ccd_data):
    """
    Add cluster metadata to the ccd_data object using the old process_cluster function.
    This method is deprecated but kept for now for comparison.
    Use the mlccd_diffusion.cluster_sigma function instead.
    """
    cluster_list = []
    df_list = []
    for i, image in enumerate(ccd_data.images):
        cluster_dict, cluster_df = process_cluster(image, i)
        cluster_list.append(cluster_dict)
        df_list.append(cluster_df)

    combined_df = pd.concat(df_list, ignore_index=True)
    merged_df = pd.merge(
        ccd_data.tracks_metadata, combined_df, left_index=True, right_index=True
    )
    ccd_data.tracks_metadata = merged_df
    return cluster_list

def add_cluster_metadata(ccd_data, threshold=None):
    """
    Add cluster metadata to the ccd_data object using the improved cluster_sigma function.
    If threshold is None, use the default threshold of 4 times the Fermilab noise level.
    """
    if threshold is None:
        threshold = 4 * mlccd_diffusion.fermilab_noise_level(unit='keV')
    sigma_x, sigma_y, energy = mlccd_diffusion.cluster_sigma(ccd_data.images[:,:,:,0], threshold=threshold, min_pixels_in_cluster=0)
    ccd_data.tracks_metadata["clusterSigmaX"] = sigma_x
    ccd_data.tracks_metadata["clusterSigmaY"] = sigma_y
    ccd_data.tracks_metadata["clusterEnergy"] = energy
    ccd_data.tracks_metadata["clusterMinSigma"] = np.minimum(sigma_x, sigma_y)
    ccd_data.tracks_metadata["clusterMaxSigma"] = np.maximum(sigma_x, sigma_y)
    return ccd_data