import os
import pickle
import numpy as np
import mlccd_models


class CCDDataLegacy:
    def __init__(self, file_path, threshold_low=0, threshold_high=2):
        self.file_path = file_path
        self.load_data()
        self.threshold_high = threshold_high
        self.threshold_low = threshold_low
        self.reshape_data()
        self.normalize_data()

    def __str__(self):
        return f"CCDData with {self.x_train.shape[0]} training samples and {self.x_test.shape[0]} testing samples"

    def __repr__(self):
        return f"CCDData(file_path={self.file_path}, training_samples={self.x_train.shape[0]}, testing_samples={self.x_test.shape[0]})"

    def __eq__(self, other: object) -> bool:
        if not type(self) == type(other):
            return False
        return (
            self.file_path == other.file_path
            and np.array_equal(self.x_train, other.x_train)
            and np.array_equal(self.x_test, other.x_test)
            and np.array_equal(self.y_train, other.y_train)
            and np.array_equal(self.y_test, other.y_test)
            and self.threshold_high == other.threshold_high
            and self.threshold_low == other.threshold_low
        )

    def load_data(self):
        with open(self.file_path, "rb") as f:
            loaded_data = pickle.load(f)
        self.x_train = loaded_data["x_train"]
        self.x_test = loaded_data["x_test"]
        self.y_train = loaded_data["y_train"]
        self.y_test = loaded_data["y_test"]
        (_, self.IMAGE_WIDTH, self.IMAGE_HEIGHT) = self.x_train.shape
        # since the CCD images are grayscale, the number of channels is 1
        self.IMAGE_CHANNELS = 1

    def reshape_data(self):
        self.x_train = self.x_train.reshape(
            self.x_train.shape[0],
            self.IMAGE_WIDTH,
            self.IMAGE_HEIGHT,
            self.IMAGE_CHANNELS,
        )

        self.x_test = self.x_test.reshape(
            self.x_test.shape[0],
            self.IMAGE_WIDTH,
            self.IMAGE_HEIGHT,
            self.IMAGE_CHANNELS,
        )

    def normalize_data(self):
        keVperADC_guillermo = 1.02857e-5
        self.x_train_kev = self.x_train * keVperADC_guillermo
        self.x_test_kev = self.x_test * keVperADC_guillermo

        x_train_kev_clipped = np.clip(
            self.x_train_kev, self.threshold_low, self.threshold_high
        )
        x_test_kev_clipped = np.clip(
            self.x_test_kev, self.threshold_low, self.threshold_high
        )
        self.x_train = (x_train_kev_clipped - self.threshold_low) / (
            self.threshold_high - self.threshold_low
        )
        self.x_test = (x_test_kev_clipped - self.threshold_low) / (
            self.threshold_high - self.threshold_low
        )

    def training_class_weights(self):
        """Calculate class weights for the training data."""
        class_counts = np.bincount(self.y_train.astype(int))
        num_negatives = class_counts[0]
        num_positives = class_counts[1]
        return {0: 1, 1: num_negatives / num_positives}

    @property
    def y_test_inverted(self):
        return 1 - self.y_test

    @property
    def y_train_inverted(self):
        return 1 - self.y_train

    @property
    def x_test_bkg_indices(self):
        return np.where(self.y_test == 0)[0].tolist()

    @property
    def x_test_tritium_indices(self):
        return np.where(self.y_test == 1)[0].tolist()

    @property
    def x_test_bkg(self):
        return self.x_test[self.x_test_bkg_indices]

    @property
    def x_test_tritium(self):
        return self.x_test[self.x_test_tritium_indices]

    @property
    def x_train_bkg_indices(self):
        return np.where(self.y_train == 0)[0].tolist()

    @property
    def x_train_tritium_indices(self):
        return np.where(self.y_train == 1)[0].tolist()

    @property
    def x_train_bkg(self):
        return self.x_train[self.x_train_bkg_indices]

    @property
    def x_train_tritium(self):
        return self.x_train[self.x_train_tritium_indices]

    def plot(self, index=1):
        mlccd_models.model_plots.side_by_side(
            self.x_train_kev,
            self.x_train,
            title_left="keV",
            title_right="Normalized",
            i=index,
        )
