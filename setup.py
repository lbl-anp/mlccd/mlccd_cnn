from setuptools import setup, find_packages

setup(
    name="mlccd_models",
    version="0.5.0",
    packages=find_packages(),
    install_requires=[
        "tensorflow",
        "numpy",
        "matplotlib",
        "seaborn",
        "scikit-learn",
    ],
    python_requires=">=3.6",
    description="Machine Learning for CCD Data Analysis",
    author="Emil Rofors",
    author_email="erofors@lbl.gov",
)
