import mlccd_models
import numpy as np
import matplotlib.pyplot as plt
import pickle
from tensorflow.keras.models import load_model


class CCDTrainer:
    def __init__(self, model, ccd_data, config=None):
        self.model = model
        self.ccd_data = ccd_data
        self.config = config
        self._predictions = None

    def __repr__(self):
        return f"CCDTrainer(model={self.model}, ccd_data={self.ccd_data}, config={self.config})"

    def write(self, filename):
        self.model.save(filename + ".keras")
        # Set the model attribute to None and pickle the rest of the object
        model = self.model
        self.model = None
        with open(filename + ".pkl", "wb") as f:
            pickle.dump(self, f)
        # Restore the model attribute
        self.model = model

    @staticmethod
    def read(filename):
        loaded_model = load_model(
            filepath=filename + ".keras",
        )
        with open(filename + ".pkl", "rb") as f:
            trainer = pickle.load(f)
        trainer.model = loaded_model
        return trainer

    def __eq__(self, other):
        if type(self).__name__ == type(other).__name__:
            return (
                self.model.get_config() == other.model.get_config()
                and self.ccd_data == other.ccd_data
                and self.config == other.config
            )
        return False

    def train(self):
        mlccd_models.train.train(self.model, self.ccd_data, self.config)

    @property
    def predictions(self):
        if self._predictions is None:
            self._predictions = self.model.predict(self.ccd_data.x_test)
        return self._predictions

    def plot_decoded(self, index=0):
        """Plot the [index]th background and tritium sample in the test set
        and its autoencoded version."""
        idx_bkg = self.ccd_data.x_test_bkg_indices[index]
        idx_tritium = self.ccd_data.x_test_tritium_indices[index]
        mlccd_models.side_by_side(
            self.ccd_data.x_test,
            self.predictions,
            idx_bkg,
            title_left="Measured BKG",
            title_right="Autoencoded",
        )
        mlccd_models.side_by_side(
            self.ccd_data.x_test,
            self.predictions,
            idx_tritium,
            title_left="Measured Tritium",
            title_right="Autoencoded",
        )


    def mse_scan(self):
        fig = plt.figure(figsize=(4, 3), dpi=150)
        thresholds = np.logspace(
            np.log10(self.mse_tritium.min()), np.log10(self.mse_tritium.max()), 100
        )
        tps = np.asarray(
            [sum(self.mse_tritium < t) / len(self.mse_tritium) for t in thresholds]
        )
        fps = np.asarray(
            [sum(self.mse_bkg > t) / len(self.mse_bkg) for t in thresholds]
        )
        plt.plot(thresholds, fps, label="False positives")
        plt.plot(thresholds, tps, label="True positives")
        plt.xscale("log")
        plt.xlabel("MSE threshold")
        plt.ylabel("Fraction")
        plt.legend()
