# mlccd_models

This python package builds, trains, and evaluates various models for the Machine Learning for CCD (MLCCD) project.

## MLCCD repos
- [mlccd_geant4_sim](https://gitlab.com/lbl-anp/mlccd/mlccd_geant4_sim) simulates particles' energy deposition in a CCD.
- [mlccd_diffusion](https://gitlab.com/lbl-anp/mlccd/mlccd_diffusion) diffuses the tracks to realistic CCD images.
- [mlccd_models](https://gitlab.com/lbl-anp/mlccd/mlccd_models) uses the prepared images to train models that can distinguish images coming from tritium to images that are not.

## Getting started
- Create a conda environment and install the required packages
```bash
conda create --name cnn python==3.11 ipython jupyter
conda activate cnn
pip install -r requirements.txt 
pip install -e .
```
An optional step for Nvidia GPU is to also 
```bash
pip install tensorflow[and-cuda]
```

- Create an account / log in to [Weights and biases](https://wandb.ai/home).
- Ask Emil Rofors to be added to the Weights and Biases ANP group.
- Go to `User settings` -> `Danger zone` and copy the API key and set the environment variable  `WANDB_API_KEY` on your system. On ubuntu you can 
```bash
echo "export WANDB_API_KEY=12345yourkeyhere12345" >> ~/.bashrc
```

- **Optional :** To use the GPU see [this](https://www.tensorflow.org/install/pip) page. For linux run
```bash
python3 -m pip install --extra-index-url https://pypi.nvidia.com tensorrt-bindings==8.6.1 tensorrt-libs==8.6.1
python3 -m pip install -U tensorflow[and-cuda]
```

- Copy the training data from google drive [here](https://drive.google.com/drive/folders/10VKPGk85HOJbMjAARlP9GRYZxddBQQyG?usp=drive_link) into the `data` folder.

- Then run the notebooks in the `notebooks` directory.
```bash
python tritium_recognition_cnn.py
```