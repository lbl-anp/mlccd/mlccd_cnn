import numpy as np
import wandb
import mlccd_models
from tensorflow.keras.callbacks import Callback

class EpochEvaluationCallback(Callback):
    def __init__(self, x_test, y_test):
        super(EpochEvaluationCallback, self).__init__()
        self.x_test = x_test
        self.y_test = y_test

    def on_epoch_end(self, epoch, logs=None):
        y_pred = self.model.predict(self.x_test)
        roc_auc_score = mlccd_models.roc_auc(self.y_test, y_pred).numpy()
        wandb.log({"roc_auc": roc_auc_score}, commit=False)

class PostTrainingEvaluationCallback(Callback):
    def __init__(self, x_test, y_test):
        super(PostTrainingEvaluationCallback, self).__init__()
        self.x_test = x_test
        self.y_test = y_test

    def on_train_end(self, logs=None):
        positive_scores = self.model.predict(self.x_test).squeeze()
        negative_scores = 1 - positive_scores
        wandb_predictions = np.column_stack((negative_scores, positive_scores))

        final_roc_auc = mlccd_models.roc_auc(self.y_test, positive_scores)
        mcc_score, mcc_threshold = mlccd_models.scan_mcc(self.y_test, positive_scores)
        final_min_mda, mda_threshold = mlccd_models.scan_mda(self.y_test, positive_scores)

        fig, ax  = mlccd_models.plot_scores(self.y_test, positive_scores, threshold_mcc_optimal=mcc_threshold, threshold_mda_optimal=mda_threshold)
        wandb.log({"score_histogram": wandb.Image(fig)}, commit=False)
        wandb.log(
            {
                "final_loss": logs["loss"],
                "final_val_loss": logs["val_loss"],
                "final_mcc_score": mcc_score,
                "final_roc_auc": final_roc_auc,
                "final_min_mda": final_min_mda,
                "roc": wandb.plot.roc_curve(
                    self.y_test,
                    wandb_predictions,
                    labels=["Background", "Tritium"],
                    classes_to_plot=[1],
                ),
            }
        )
