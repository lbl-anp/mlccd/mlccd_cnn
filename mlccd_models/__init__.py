# This file makes the directory a Python package

from .ccd_data import CCDData
from .ccd_data_legacy import CCDDataLegacy
from .models import VAE, VAEModel, AutoencoderModel, CNNModel
from .metrics import (
    F1Score,
    specificity,
    roc_auc,
    confusion_matrix,
    calculate_mcc,
    scan_mcc,
    calculate_mda,
    scan_mda,
)
from .model_plots import (
    side_by_side,
    plot_confusion_matrix,
    plot_prediction_grid,
    plot_roc_curve,
    plot_12,
    plot_scores,
    plot_confusion_matrix_clusters,
)
from .classical_utils import (
    add_cluster_metadata,
    add_cluster_metadata_legacy,
)
from .train import train
from .evaluators import PostTrainingEvaluationCallback, EpochEvaluationCallback
from .ccd_trainer import CCDTrainer
