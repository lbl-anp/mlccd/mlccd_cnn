import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import datetime
import mlccd_models
import os
import mlccd_diffusion

# deactivate GPU on ragnarok
tf.config.set_visible_devices([], "GPU")

# %%
# Load the training and test data
data_tritium = mlccd_models.CCDData(
    # os.path.expanduser("~/repos/mlccd_geant4_sim/Geant4_CCD_simulation/output/diffused_H3_5000000.h5"),
    # os.path.expanduser("~/data/CCD_data/Simulated_training_clusters/2024-10-01-tritium-and-gamma-100keV-more-metadata/diffused_H3_5000000.h5"),
    os.path.expanduser("~/data/CCD_data/Simulated_training_clusters/2024-10-17-tritium-and-gamma-100keV-more-realistic-diffusion/tritium_10_17_add_classical_disc.h5"),
    image_unit="keV"
)
data_gamma = mlccd_models.CCDData(
    # os.path.expanduser("~/repos/mlccd_geant4_sim/Geant4_CCD_simulation/output/diffused_gamma_20000000.h5"),
    # os.path.expanduser("~/data/CCD_data/Simulated_training_clusters/2024-10-01-tritium-and-gamma-100keV-more-metadata/diffused_gamma_20000000.h5"),
    os.path.expanduser("~/data/CCD_data/Simulated_training_clusters/2024-10-17-tritium-and-gamma-100keV-more-realistic-diffusion/gamma_10_17_add_classical_disc.h5"),
    image_unit="keV"
)

# %%
def mda_at_num_events(num_events, seed):
    tf.random.set_seed(seed)
    np.random.seed(seed)
    # Combine the two datasets
    ccd_data = 1 * data_gamma + 1 * data_tritium
    # Clip the data between thresholds and normalize to 0-1
    ccd_data.normalize(threshold_low=0, threshold_high=20)
    ccd_data = ccd_data.reduced(num_events=num_events)
    # Divide the data into training, test, and validation sets
    ccd_data.divide_data(
        train_fraction=0.7, test_fraction=0.2, validation_fraction=0.1
    )
    gamma = 2
    loss_function = tf.keras.losses.BinaryFocalCrossentropy(
        gamma=gamma, from_logits=False, apply_class_balancing=True
    )
    # loss_function = 'binary_crossentropy'
    config = {
        "model_name": f'cnn_{datetime.datetime.now().strftime("%Y%m%d_%H%M")}',
        "learning_rate": 0.001,
        "architecture": "CNN",
        "dataset": ccd_data.file_path,
        "epochs": 25,
        "batch_size": 256 * 4,
        "loss_function": loss_function,
        "threshold_high": ccd_data.threshold_high,
        "threshold_low": ccd_data.threshold_low,
        "data_filename": ccd_data.file_path,
    }
    num_bkg = sum(ccd_data.labels == 0)
    num_tritium = sum(ccd_data.labels == 1)
    print(f"Number of background events: {num_bkg}")
    print(f"Number of tritium events: {num_tritium}")

    # Create the model
    model = mlccd_models.CNNModel(
        ccd_data.IMAGE_WIDTH,
        ccd_data.IMAGE_HEIGHT,
        ccd_data.IMAGE_CHANNELS,
    )

    adam_optimizer = tf.keras.optimizers.Adam(
        learning_rate=config["learning_rate"]
    )

    model.compile(optimizer=adam_optimizer, loss=config["loss_function"])
    mlccd_models.train(model=model, config=config, ccd_data=ccd_data, offline=True)
    y_pred = model.predict(ccd_data.x_test)
    classical_discriminator_scores = ccd_data.tracks_metadata.iloc[ccd_data.test_indices].classicalDiscriminatorS

    min_mda, mda_threshold = mlccd_models.scan_mda(ccd_data.y_test, y_pred, plot=False)
    return min_mda

# %%
def mda_at_num_events_autoencoder(num_events, seed):
    tf.random.set_seed(seed)
    np.random.seed(seed)
    # Combine the two datasets
    ccd_data = 1 * data_gamma + 1 * data_tritium
    # Clip the data between thresholds and normalize to 0-1
    ccd_data.normalize(threshold_low=0, threshold_high=20)
    ccd_data = ccd_data.reduced(num_events=num_events)
    # Divide the data into training, test, and validation sets
    ccd_data.divide_data(
        train_fraction=0.7, test_fraction=0.2, validation_fraction=0.1
    )
    gamma = 2
    loss_function = tf.keras.losses.BinaryFocalCrossentropy(
        gamma=gamma, from_logits=False, apply_class_balancing=True
    )
    loss_function = "binary_crossentropy"
    loss_function = "mean_squared_error"
    pinch_size = 6
    kl_loss_weight = 0
    l1_weight = 2
    mse_scaler = 5000
    config = {
        "model_name": f'autoencoder_{datetime.datetime.now().strftime("%Y%m%d_%H%M")}',
        "learning_rate": 0.001,
        "architecture": "autoencoder",
        "dataset": ccd_data.file_path,
        "epochs": 4,
        "batch_size": 256,
        "loss_function": loss_function,
        "threshold_high": ccd_data.threshold_high,
        "threshold_low": ccd_data.threshold_low,
        "data_filename": ccd_data.file_path,
        "pinch_size": pinch_size,
        "kl_loss_weight": kl_loss_weight,
        "l1_weight": l1_weight,
        "padding": "same",
        "mse_scaler": mse_scaler,
    }
    # Create the model
    model = mlccd_models.VAEModel(
        ccd_data.IMAGE_WIDTH,
        ccd_data.IMAGE_HEIGHT,
        ccd_data.IMAGE_CHANNELS,
        pinch_size=config["pinch_size"],
        kl_loss_weight=config["kl_loss_weight"],
        l1_weight=config["l1_weight"],
        padding=config["padding"],
        mse_scaler=config["mse_scaler"],
    )

    adam_optimizer = tf.keras.optimizers.Adam(
        learning_rate=config["learning_rate"]
    )

    model.compile(optimizer=adam_optimizer, loss=config["loss_function"])
    mlccd_models.train(model=model, config=config, ccd_data=ccd_data, offline=True)
    y_pred = model.predict(ccd_data.x_test)
    min_mda, mda_threshold = mlccd_models.scan_mda(ccd_data.y_test, y_pred, plot=False)
    return min_mda

# %%
sizes = np.geomspace(500, 2e6, num=30).astype(int)  # Reduced number of sizes for faster computation
num_runs = 3

mda_at_sizes_autoencoder = []
mda_at_sizes = []

for size in sizes:
    mda_autoencoder_runs = []
    mda_cnn_runs = []
    for run in range(num_runs):
        seed = run * 1000 + size  # Create a unique seed for each run and size
        mda_autoencoder_runs.append(mda_at_num_events_autoencoder(size, seed))
        mda_cnn_runs.append(mda_at_num_events(size, seed))
    mda_at_sizes_autoencoder.append(mda_autoencoder_runs)
    mda_at_sizes.append(mda_cnn_runs)

# %%
import pickle
with open("mda_at_sizes_autoencoder_multiple.pkl", "wb") as f:
    pickle.dump(mda_at_sizes_autoencoder, f)
with open("mda_at_sizes_multiple.pkl", "wb") as f:
    pickle.dump(mda_at_sizes, f)
with open("sizes_multiple.pkl", "wb") as f:
    pickle.dump(sizes, f)

# %%
# load the data
with open("mda_at_sizes_autoencoder_multiple.pkl", "rb") as f:
    mda_at_sizes_autoencoder = pickle.load(f)
with open("mda_at_sizes_multiple.pkl", "rb") as f:
    mda_at_sizes = pickle.load(f)
with open("sizes_multiple.pkl", "rb") as f:
    sizes = pickle.load(f)

# %%
# Convert to numpy arrays and handle None values
mda_at_sizes_autoencoder = np.array([[0 if i is None else i for i in run] for run in mda_at_sizes_autoencoder])
mda_at_sizes = np.array([[0 if i is None else i for i in run] for run in mda_at_sizes])

# Calculate mean and standard deviation
mda_autoencoder_mean = np.mean(mda_at_sizes_autoencoder, axis=1)
mda_autoencoder_std = np.std(mda_at_sizes_autoencoder, axis=1)
mda_cnn_mean = np.mean(mda_at_sizes, axis=1)
mda_cnn_std = np.std(mda_at_sizes, axis=1)

# %%
sizes = np.array(sizes)
training_fraction = 0.7

plt.figure(figsize=(3.5,2.5), dpi=150)

# Plot Autoencoder results
plt.errorbar(sizes*training_fraction/2, mda_autoencoder_mean*1000, 
             yerr=mda_autoencoder_std*1000, label="Autoencoder", 
             capsize=3, capthick=1, fmt='o-', markersize=4)

# Plot CNN results
plt.errorbar(sizes*training_fraction, mda_cnn_mean*1000, 
             yerr=mda_cnn_std*1000, label="CNN", 
             capsize=3, capthick=1, fmt='s-', markersize=4)

plt.xlabel("# training events")
plt.ylabel("MDA (mBq)")
plt.axhline(y=0.00623*1000, color='C2', linestyle='--', label='Classical discriminator', zorder=0)
plt.xscale("log")
plt.xlim(50,10000000)
plt.legend()
plt.tight_layout()
plt.show()




