import tensorflow as tf
from tensorflow.keras import metrics
from tensorflow.keras import backend as K
import keras
from tensorflow.keras.metrics import AUC
from tensorflow.keras.utils import register_keras_serializable
import numpy as np
import matplotlib.pyplot as plt


class F1Score(tf.keras.metrics.Metric):
    def __init__(self, name="f1_score", **kwargs):
        super().__init__(name=name, **kwargs)
        self.precision = tf.keras.metrics.Precision()
        self.recall = tf.keras.metrics.Recall()

    def update_state(self, y_true, y_pred, sample_weight=None):
        self.precision.update_state(y_true, y_pred, sample_weight)
        self.recall.update_state(y_true, y_pred, sample_weight)

    def result(self):
        precision = self.precision.result()
        recall = self.recall.result()
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

    def reset_state(self):
        self.precision.reset_state()
        self.recall.reset_state()


# Define Specificity or True Negative Rate metric
@register_keras_serializable()
def specificity(y_true, y_pred):
    neg_y_true = 1 - y_true
    neg_y_pred = 1 - y_pred
    tn = K.sum(neg_y_true * neg_y_pred)
    fp = K.sum(neg_y_true * (1 - neg_y_pred))
    return tn / (tn + fp + K.epsilon())


def calculate_mcc(y_true, y_pred, threshold=0.5):
    """
    Calculates the Matthews Correlation Coefficient (MCC) for a given threshold.
    A higher MCC value indicates a better model.
    
    Parameters
    ----------
    y_true : array
        True labels. (0: background, 1: tritium)
    y_pred : array
        Predicted scores. (low scores: background, high scores: tritium)
    threshold : float
        Threshold value for binary classification. Above this value, the
        prediction is considered as tritium.
    
    Returns
    -------
    mcc : float
        Matthews Correlation Coefficient.
    """
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, threshold)
    numerator = tp * tn - fp * fn
    denominator = np.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))
    return numerator / (denominator + np.finfo(float).eps)


def confusion_matrix(y_true, y_pred, threshold=0.5):
    binary_predictions = (y_pred >= threshold).astype(int)
    confusion_matrix = np.zeros((2, 2))
    for t, p in zip(y_true, binary_predictions):
        confusion_matrix[t, p] += 1
    tn = confusion_matrix[0, 0]
    fp = confusion_matrix[0, 1]
    fn = confusion_matrix[1, 0]
    tp = confusion_matrix[1, 1]
    return tn, fp, fn, tp


def scan_mcc(y_true, y_pred, plot=False):
    """
    Scans all thresholds and returns the highest Matthews Correlation Coefficient 
    (MCC) value and the corresponding threshold.

    Parameters
    ----------
    y_true : array
        True labels. (0: background, 1: tritium)
    y_pred : array
        Predicted scores. (low scores: background, high scores: tritium)
    plot : bool
        If True, a plot of the MCC values as a function of the threshold is shown.

    Returns
    -------
    max_mcc : float
        Maximum MCC value.
    optimal_threshold : float
        The threshold that gives the maximum MCC value.
    """
    def mcc_at_threshold(threshold):
        return calculate_mcc(y_true, y_pred, threshold)

    # Define thresholds to test
    thresholds = np.linspace(np.min(y_pred), np.max(y_pred), 25)
    # Calculate MCC for each threshold
    mcc_values = np.array([mcc_at_threshold(threshold) for threshold in thresholds])
    max_mcc = np.max(mcc_values)
    optimal_threshold = thresholds[np.argmax(mcc_values)]
    if plot:
        plt.figure(figsize=(4, 3), dpi=150)
        plt.plot(thresholds, mcc_values, marker="o")
        plt.scatter(
            optimal_threshold,
            max_mcc,
            color="red",
            label=f"Best MCC = {max_mcc:.2f}",
            zorder=10,
        )
        plt.xlabel("Threshold")
        plt.ylabel("MCC")
        plt.legend()
        plt.show()
    # Return the maximum MCC value
    return max_mcc, optimal_threshold


# Define ROC AUC metric
@register_keras_serializable()
def roc_auc(y_true, y_pred):
    auc = tf.metrics.AUC(name="auc")
    auc.update_state(y_true, y_pred)
    return auc.result()


def calculate_mda(
    y_true,
    y_pred,
    threshold,
    measurement_time=3600 * 24,
    bkg_activity=5000 / 25 / 60 / 60,
    deadlayer_loss=0.48,
    solid_angle_loss=0.5,
    use_currie=True,
):
    """
    Calculates the minimum detectable activity (MDA) for a given threshold.
    If the threshold is set so that less than 10 images are classified as
    true positive, false positive, true negative or false negative, the
    MDA calculation is unreliable and None is returned to indicate
    that no MDA could be calculated.

    Parameters
    ----------
    y_true : array
        True labels. (0: background, 1: tritium)
    y_pred : array
        Predicted scores. (low scores: background, high scores: tritium)
    threshold : float
        Threshold value for binary classification. Above this value, the
        prediction is considered as tritium.
    measurement_time : float
        Measurement time in seconds. Default is 24 hours.
    bkg_activity : float
        Background activity in Bq. Default is 5000 counts in 25 hours, based on a "classical" cut
        on the Fermilab CCD background measurement.
    deadlayer_loss : float
        Deadlayer loss factor. Default is 0.48 which comes from that 48% of the
        tritium clusters are lost in a 100 nm deadlayer in our Geant4 simulations.
    solid_angle_loss : float
        Solid angle loss factor. Default is 0.5 which comes from the fact that
        50% of the emitted beta particles are lost due to the solid angle of the
        CCD.
    use_currie : bool
        If True, use the Currie equation for MDA calculation. If False, use the
        3-sigma method. Default is True.

    Returns
    -------
    mda : float
        Minimum detectable activity in Bq.
    """
    tn, fp, fn, tp = confusion_matrix(y_true, y_pred, threshold)
    # Check if MDA calculation is possible
    if tn < 10 or fp < 10 or fn < 10 or tp < 10:
        return None
    tp_rate = tp / (tp + fn)
    fp_rate = fp / (fp + tn)
    fp_during_measurement = bkg_activity * fp_rate * measurement_time
    
    if use_currie:
        # Currie equation: Minimum signal yield = 4.653 * sigma_bkg + 2.706
        sigma_bkg = np.sqrt(fp_during_measurement)
        min_signal_yield = 4.653 * sigma_bkg + 2.706
    else:
        # 3-sigma method
        min_signal_yield = 3 * np.sqrt(fp_during_measurement)
    
    mda = min_signal_yield / (tp_rate * measurement_time)
    # Apply corrections for deadlayer and solid angle
    mda = mda / (1 - deadlayer_loss) / (1 - solid_angle_loss)
    return mda


def scan_mda(y_true, y_pred, plot=False, use_currie=True, **kwargs):
    """Scans all thresholds and returns the lowest MDA and the corresponding threshold.

    Parameters
    ----------
    y_true : array
        True labels. (0: background, 1: tritium)
    y_pred : array
        Predicted scores. (low scores: background, high scores: tritium)
    plot : bool
        If True, a plot of the MDA values as a function of the threshold is shown.
    use_currie : bool
        If True, use the Currie equation for MDA calculation. If False, use the
        3-sigma method. Default is True.
    **kwargs
        Additional keyword arguments for calculate_mda.

    Returns
    -------
    min_mda : float
        Minimum detectable activity in Bq.
    optimal_threshold : float
        The threshold that gives the minimum MDA.
    """
    thresholds = np.linspace(np.min(y_pred), np.max(y_pred), 25)
    mda_values = np.array(
        [calculate_mda(y_true, y_pred, threshold, use_currie=use_currie, **kwargs) for threshold in thresholds]
    )
    valid_indices = [i for i, mda in enumerate(mda_values) if mda is not None]
    if not valid_indices:
        return None, None

    min_mda = np.min([mda_values[i] for i in valid_indices])
    optimal_threshold = thresholds[valid_indices[np.argmin([mda_values[i] for i in valid_indices])]]
    
    if plot:
        plt.figure(figsize=(4, 3), dpi=150)
        plt.plot(
            thresholds[mda_values != None], [mda for mda in mda_values if mda is not None], marker="o"
        )
        plt.scatter(
            optimal_threshold,
            min_mda,
            color="red",
            label=f"Best MDA = {min_mda*1000:.2f}mBq",
            zorder=10,
        )
        plt.xlabel("Threshold")
        plt.ylabel("MDA (Bq)")
        plt.title(f"MDA vs Threshold ({'Currie' if use_currie else '3-sigma'})")
        plt.legend()
        plt.show()
    return min_mda, optimal_threshold
