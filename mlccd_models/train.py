import wandb
import mlccd_models
import tensorflow as tf

def generate_model(config, data):
    adam_optimizer = tf.keras.optimizers.Adam(
        learning_rate=config["learning_rate"]
    )
    if config.model_name == "VAE":
        model = mlccd_models.VAEModel(
            data.IMAGE_WIDTH,
            data.IMAGE_HEIGHT,
            data.IMAGE_CHANNELS,
            config.VAE['pinch_size'],
            config.VAE['kl_loss_weight'],
            config.VAE['l1_weight'],
            config.VAE['padding'],
        )
        loss_function = config.VAE['loss_function']
    elif config.model_name == "CNN":
        model = mlccd_models.CNNModel(
            data.IMAGE_WIDTH,
            data.IMAGE_HEIGHT,
            data.IMAGE_CHANNELS,
        )
        loss_function = config.CNN['loss_function']
    model.compile(
        optimizer=adam_optimizer,
        loss=loss_function,
    )
    return model


def train(model=None, ccd_data=None, config=None, offline=False):
    # Initialize wandb
    if not offline:
        wandb.init()
    if config is None:
        config = wandb.config

    if ccd_data is None:
        # Load the training and test data
        data_tritium = mlccd_models.CCDData(config.tritium_data_path, seed=config.seed)
        data_gamma = mlccd_models.CCDData(config.gamma_data_path, seed=config.seed)
        # reduce to set number of events
        data_tritium.reduced(config.num_tritium, in_place=True)
        data_gamma.reduced(config.num_gamma, in_place=True)
        # Combine the two datasets
        ccd_data = 1 * data_gamma + 1 * data_tritium
        # Clip the data between thresholds and normalize to 0-1
        ccd_data.normalize(threshold_low=config.threshold_low, threshold_high=config.threshold_high)
        # Divide the data into training, test, and validation sets
        ccd_data.divide_data(
            train_fraction=config["train_fraction"], 
            test_fraction=config["test_fraction"], 
            validation_fraction=config["validation_fraction"]
        )
    if offline:
        wandb_metrics_logger = None
    else:
        # Define wandb callbacks
        wandb_metrics_logger = wandb.keras.WandbMetricsLogger(log_freq="epoch")

    if model is None:
        model = generate_model(config, ccd_data)

    # Create end of epoch evaluation callback
    evaluation_callback = mlccd_models.EpochEvaluationCallback(
        x_test=ccd_data.x_test, y_test=ccd_data.y_test
    )
    # Create post-training evaluation callback
    post_training_evaluation_callback = mlccd_models.PostTrainingEvaluationCallback(
        x_test=ccd_data.x_test, y_test=ccd_data.y_test
    )
    if offline:
        callbacks = None
    else:
        callbacks = [wandb_metrics_logger, evaluation_callback, post_training_evaluation_callback]

    training_history = model.fit(
        ccd_data.x_train,
        ccd_data.y_train,
        epochs=config["epochs"],
        batch_size=config["batch_size"],
        validation_data=(ccd_data.x_test, ccd_data.y_test),
        callbacks=callbacks,
    )


    # Finish the wandb run
    if not offline:
        wandb.finish()

if __name__ == '__main__':
    train()
